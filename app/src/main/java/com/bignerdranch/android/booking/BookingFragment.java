package com.bignerdranch.android.booking;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

import static android.widget.CompoundButton.*;

/**
 * Created by jeremy on 3/21/18.
 */

public class BookingFragment extends Fragment {

    private static final String ARG_BOOKING_ID = "booking_id";
    private static final String DIALOG_DATE = "DialogDate";

    private Booking mBooking;
    private EditText mFirstNameField, mLastNameField;
    private Button mCheckInButton, mCheckOutButton;
    private CheckBox mInternetCheckBox;

    public static BookingFragment newInstance(UUID bookingId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_BOOKING_ID, bookingId);
        BookingFragment fragment = new BookingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID bookingID = (UUID) getArguments().getSerializable(ARG_BOOKING_ID);
        mBooking = BookLab.get(getActivity()).getBooking(bookingID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_booking, container, false);

        mFirstNameField = (EditText) v.findViewById(R.id.first_name);
        mFirstNameField.setText(mBooking.getFirstName());
        mFirstNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBooking.setFirstName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mLastNameField = (EditText) v.findViewById(R.id.last_name);
        mLastNameField.setText(mBooking.getLastName());

        mLastNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int
                    after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBooking.setLastName(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCheckInButton = (Button) v.findViewById(R.id.check_in_date);
        mCheckInButton.setText(mBooking.getCheckInDate().toString());
//        mCheckInButton.setEnabled(false);
        mCheckInButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = new DatePickerFragment().newInstance(mBooking.getCheckInDate());
                dialog.show(manager, DIALOG_DATE);
            }
        });
        mCheckOutButton = (Button) v.findViewById(R.id.check_out_date);
        mCheckOutButton.setText(mBooking.getCheckOutDate().toString());
        mCheckOutButton.setEnabled(false);


        mInternetCheckBox = (CheckBox) v.findViewById(R.id.include_internet) ;
        mInternetCheckBox.setChecked(mBooking.isInternet());
        mInternetCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
            mBooking.setInternet(isChecked);
            }
        });
        return v;

    }
}
