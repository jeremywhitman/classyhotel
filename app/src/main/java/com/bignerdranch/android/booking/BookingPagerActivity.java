package com.bignerdranch.android.booking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

/**
 * Created by jeremy on 3/22/18.
 */

public class BookingPagerActivity extends AppCompatActivity {
    private static final String EXTRA_BOOKING_ID = "com.bignerdranch.android.classyhotel" +
            ".booking_id";

    private ViewPager mViewPager;
    private List<Booking> mBookings;

    public static Intent newIntent(Context packageContext, UUID bookingId) {
        Intent intent = new Intent(packageContext, BookingPagerActivity.class);
        intent.putExtra(EXTRA_BOOKING_ID, bookingId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_pager);

        UUID bookingId = (UUID) getIntent().getSerializableExtra(EXTRA_BOOKING_ID);

        mViewPager = (ViewPager) findViewById(R.id.booking_view_pager);

        mBookings = BookLab.get(this).getBooks();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Booking booking = mBookings.get(position);
                return BookingFragment.newInstance(booking.getId());
            }

            @Override
            public int getCount() {
                return mBookings.size();
            }
        });

        for (int i = 0; i<mBookings.size(); i++){
            if (mBookings.get(i).getId().equals(bookingId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
