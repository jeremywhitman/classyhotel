package com.bignerdranch.android.booking;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class BookingListFragment extends Fragment {
    private RecyclerView mBookingRecyclerView;
    private BookingAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_list, container, false);
        mBookingRecyclerView = (RecyclerView) view.findViewById(R.id.booking_recycler_view);
        mBookingRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        BookLab bookLab = BookLab.get(getActivity());
        List<Booking> bookings = bookLab.getBooks();

        if (mAdapter == null) {
            mAdapter = new BookingAdapter(bookings);
            mBookingRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }

    }

    private class BookingHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private Booking mBook;

        private TextView mFirstNameTextView;
        private TextView mLastNameTextView;
        private TextView mCheckInDateTextView;
        private TextView mCheckOutDateTextView;
        private ImageView mWithInternetImageView;

        public BookingHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_booking, parent, false));
            itemView.setOnClickListener(this);

            mFirstNameTextView = (TextView) itemView.findViewById(R.id.first_name);
            mLastNameTextView = (TextView) itemView.findViewById(R.id.last_name);
            mCheckInDateTextView = (TextView) itemView.findViewById(R.id.check_in_date);
            mCheckOutDateTextView = (TextView) itemView.findViewById(R.id.check_out_date);
            mWithInternetImageView = (ImageView) itemView.findViewById(R.id.with_internet);
        }

        public void bind(Booking booking) {
            mBook = booking;

            mFirstNameTextView.setText(mBook.getFirstName());
            mLastNameTextView.setText(mBook.getLastName());
            mCheckInDateTextView.setText(mBook.getCheckInDate().toString());
            mCheckOutDateTextView.setText(mBook.getCheckOutDate().toString());
            mWithInternetImageView.setVisibility(booking.isInternet()? View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View view) {
            Intent intent = BookingPagerActivity.newIntent(getActivity(), mBook.getId());
            startActivity(intent);
        }
    }

    private class BookingAdapter extends RecyclerView.Adapter<BookingHolder> {

        private List<Booking> mBooks;

        public BookingAdapter(List<Booking> bookings) {
            mBooks = bookings;
        }

        @Override
        public BookingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
             LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
             return new BookingHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(BookingHolder holder, int position) {
            Booking booking = mBooks.get(position);
            holder.bind(booking);
        }

        @Override
        public int getItemCount() {
            return mBooks.size();
        }
    }
}
