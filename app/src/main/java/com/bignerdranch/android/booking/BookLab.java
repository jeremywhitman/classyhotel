package com.bignerdranch.android.booking;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jeremy on 3/21/18.
 */

public class BookLab {
    private static BookLab sBookLab;
    private List<Booking> mBooks;

    public static BookLab get(Context context) {
        if (sBookLab == null) {
            sBookLab = new BookLab(context);
        }
        return sBookLab;
    }

    private BookLab(Context context) {
        mBooks = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Booking booking = new Booking();
            booking.setFirstName("Bob");
            booking.setLastName("Last " + i);
            booking.setInternet(i % 2 == 0);
            mBooks.add(booking);
        }
    }

    public List<Booking> getBooks() {
        return mBooks;
    }

    public Booking getBooking(UUID id) {
        for (Booking booking : mBooks) {
            if (booking.getId().equals(id)){
                return booking;
            }
        }
        return null;
    }
}
