package com.bignerdranch.android.booking;

import java.util.Date;
import java.util.UUID;

/**
 * Created by jeremy on 3/21/18.
 */

public class Booking {
    private UUID mId;
    private String mFirstName, mLastName;
    private Date mCheckInDate, mCheckOutDate;
    private boolean mInternet;

    public Booking() {
        mId = UUID.randomUUID();
        mCheckInDate = new Date();
        mCheckOutDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Date getCheckInDate() {
        return mCheckInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        mCheckInDate = checkInDate;
    }

    public Date getCheckOutDate() {
        return mCheckOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        mCheckOutDate = checkOutDate;
    }

    public boolean isInternet() {
        return mInternet;
    }

    public void setInternet(boolean internet) {
        mInternet = internet;
    }
}
