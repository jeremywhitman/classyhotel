package com.bignerdranch.android.booking;
import android.support.v4.app.Fragment;


public class BookingListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new BookingListFragment();

    }
}
